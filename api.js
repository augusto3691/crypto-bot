const axios = require('axios');
const qs = require('querystring');
const crypto = require('crypto');
const ENDPOINT_API = 'https://www.mercadobitcoin.com.br/api/'

const ENDPOINT_TRADE_PATH = "/tapi/v3/"
const ENDPOINT_TRADE_API = 'https://www.mercadobitcoin.net' + ENDPOINT_TRADE_PATH

const infoApiInstance = axios.create({
    baseURL: ENDPOINT_API,
    headers: {
        'Accept': 'application/json',
    }
});

const tradeApiInstance = axios.create({
    baseURL: ENDPOINT_TRADE_API,
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded'
    }
});

export class MercadoBitcoin {
    constructor(config) {
        this.config = {
            CURRENCY: config.currency
        }
    }

    ticker = (success) => {
        this.call('ticker', success);
    }

    orderBook = (success) => {
        this.call('orderbook', success);
    }

    trades = (success) => {
        this.call('trades', success);
    }

    call = (method, success) => {
        infoApiInstance.get(this.config.CURRENCY + '/' + method)
            .then((response) => {
                try {
                    success(response.data);
                } catch (e) {
                    console.log(e)
                }
            })
            .catch((e) => {
                console.log(e);
            })
    }
}

export class MercadoBitcoinTrade {
    constructor(config) {
        this.config = {
            KEY: config.key,
            SECRET: config.secret,
            PIN: config.pin,
            CURRENCY: config.currency
        }
    }


    getAccountInfo = (success, error) => {
        this.call('get_account_info', {}, success, error)
    }

    getOrder = (orderId, success, error) => {
        this.call('get_order', {
            coin_pair: `BRL${this.config.CURRENCY}`,
            order_id: orderId
        }, success, error)
    }

    getBuyOpenedOrders = (success, error) => {
        this.call('list_orders', {
            order_type: 1,
            status_list: "[2]",
            coin_pair: `BRL${this.config.CURRENCY}`,
        }, success, error)
    }

    placeBuyOrder = (qty, limit_price, success, error) => {
        this.call('place_buy_order', {
            coin_pair: `BRL${this.config.CURRENCY}`,
            quantity: ('' + qty).substr(0, 10),
            limit_price: '' + limit_price
        }, success, error)
    }

    placeSellOrder = (qty, limit_price, success, error) => {
        this.call('place_sell_order', {
            coin_pair: `BRL${this.config.CURRENCY}`,
            quantity: ('' + qty).substr(0, 10),
            limit_price: ('' + limit_price).substring(0, 5) //Máximo de 5 casas do valor que esta vendendo
        }, success, error)
    }

    cancelOrder = (orderId, success, error) => {
        this.call('cancel_order', {
            coin_pair: `BRL${this.config.CURRENCY}`,
            order_id: orderId
        }, success, error)
    }

    call = (method, parameters, success, error) => {

        let now = Math.round(new Date().getTime() / 1000)
        let queryString = qs.stringify({
            'tapi_method': method,
            'tapi_nonce': now
        })
        if (parameters) queryString += '&' + qs.stringify(parameters)

        let signature = crypto.createHmac('sha512', this.config.SECRET)
            .update(ENDPOINT_TRADE_PATH + '?' + queryString)
            .digest('hex')

        tradeApiInstance.post("", queryString, {
                headers: {
                    'TAPI-ID': this.config.KEY,
                    'TAPI-MAC': signature
                }
            })
            .then((response) => {
                if (response.data) {
                    if (response.data.status_code === 100 && success)
                        success(response.data.response_data)
                    else if (error)
                        error(response.data.error_message)
                    else
                        console.log(response.data)
                }
            })
            .catch((e) => {
                console.log(e);
            })
    }

}