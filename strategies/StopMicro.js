import {
    MercadoBitcoin,
    MercadoBitcoinTrade
} from "../api";

import chalk from 'chalk';
import _ from 'lodash';

require("dotenv-safe").load();


export class StopMicro {

    constructor(config) {
        this.config = {
            currency: config.currency, // Qual a moeda
            key: process.env.KEY,
            secret: process.env.SECRET,
            pin: process.env.PIN,
            decay: config.decay, //Qual a porcentragem que se deseja comprar a menos
            invest: config.invest, //Qual o limite que a estratégia vai utilizar do seu saldo total
            exchange: config.exchange, //Qual a quantidade que ela vai compra/vender a cada ciclo da estratégia
            profit: config.profit //Qual a porcentagem que se deseja atingir de lucro
        }
        this.infoApi = new MercadoBitcoin(this.config)
        this.tradeApi = new MercadoBitcoinTrade(this.config)
    }



    start = () => {

        this.tradeApi.getAccountInfo((acountInfo) => {
            if (acountInfo.balance.brl.available < this.config.invest) {
                console.log(chalk.yellow("Aguardando saldo para operar..."));

                setTimeout(() => {
                    this.start();
                }, process.env.CRAWLER_INTERVAL)

            } else {
                this.infoApi.ticker((ticker) => {

                    let valorCompra = (Number(ticker.ticker.last / this.config.decay).toFixed(5));
                    // let valorCompra = Number(ticker.ticker.last).toFixed(5);
                    let qtdCompra = this.config.exchange / valorCompra;


                    console.log(chalk.cyan("O valor da moeda esta: ") + chalk.green("R$ " + Number(ticker.ticker.last).toFixed(5)));
                    console.log(chalk.cyan("Estou colocando uma ordem de compra de ") + chalk.green(qtdCompra) + chalk.cyan(" a: ") + chalk.green("R$ " + valorCompra + chalk.cyan(" por " + process.env.WAIT_TO_BUY / 1000 + " segundos e ver se vende...")));

                    this.tradeApi.placeBuyOrder(qtdCompra, valorCompra, (buyOrderResponse) => {
                        console.log(chalk.green("Ordem de compra " + buyOrderResponse.order.order_id + " inserida"));
                        console.log(buyOrderResponse);
                        setTimeout(() => {
                            this.tradeApi.getOrder(buyOrderResponse.order.order_id, (order) => {
                                if (order.order.status == 2) {
                                    console.log(chalk.red("Ordem " + buyOrderResponse.order.order_id + " não executada, cancelando..."));
                                    this.tradeApi.cancelOrder(buyOrderResponse.order.order_id, () => {
                                        this.start();
                                    })
                                } else {
                                    let valorVenda = Number(ticker.ticker.last * this.config.profit).toFixed(5);
                                    let qtdVenda = buyOrderResponse.order.executed_quantity - buyOrderResponse.order.fee - 0.00000001

                                    console.log(chalk.cyan("Estou colocando uma ordem de venda de ") + chalk.green(qtdVenda) + chalk.cyan(" a : ") + chalk.green("R$ " + valorVenda));
                                    this.tradeApi.placeSellOrder(qtdVenda, valorVenda, (sellOrderResponse) => {
                                        console.log(chalk.cyan("Ordem de venda " + sellOrderResponse.order.order_id + " inserida"));
                                        this.start();
                                    })

                                }
                            })
                        }, process.env.WAIT_TO_BUY)

                    }, (e) => {
                        console.log(chalk.red("Não foi possivel inserir a ordem de compra"));
                        console.log(chalk.red(e));
                    })

                })
            }
        })



    }
}