import {
    MercadoBitcoin
} from "../api";

require("dotenv-safe").load();

export class Analytic {

    constructor(config) {
        this.config = {
            currency: config.currency // Qaul a moeda
        }
        this.infoApi = new MercadoBitcoin(this.config)
    }

    start = () => {
        setInterval(() => {
            this.infoApi.ticker((ticker) => {
                console.log(ticker);
            })
        }, process.env.CRAWLER_INTERVAL)

    }
}