import {
    StopMicro
} from './strategies/StopMicro'



let strategy = new StopMicro({
    currency: "LTC",
    invest: 20,
    exchange: 5.00,
    profit: 1.02,
    decay: 1.001
})

strategy.start();

// function getQuantity(coin, price, isBuy, callback) {
//     price = parseFloat(price)
//     coin = isBuy ? 'brl' : coin.toLowerCase()

//     tradeApi.getAccountInfo((response_data) => {
//             var balance = parseFloat(response_data.balance[coin].available).toFixed(5)
//             balance = parseFloat(balance)
//             if (isBuy && balance < 1) return console.log('Sem saldo disponível para comprar!')
//             console.log(`Saldo disponível de ${coin}: ${balance}`)

//             if (isBuy) balance = parseFloat((balance / price).toFixed(5))
//             callback(parseFloat(balance) - 0.00001) //tira a diferença que se ganha no arredondamento
//         },
//         (data) => console.log(data))
// }

// setInterval(() =>
//     infoApi.ticker((response) => {
//         if (response.ticker.sell <= 60) {
//             getQuantity('BRL', response.ticker.sell, true, (qty) => {
//                 tradeApi.placeBuyOrder(qty, response.ticker.sell,
//                     (data) => {
//                         console.log('Ordem de compra inserida no livro. ' + data.order.order_id)
//                         //operando em STOP
//                         getQuantity('XRP', 0, false, (qty) => {
//                             tradeApi.placeSellOrder(qty, response.ticker.sell * parseFloat(process.env.PROFITABILITY),
//                                 (data) => console.log('Ordem de venda inserida no livro. ' + data.order.order_id),
//                                 (data) => console.log('Erro ao inserir ordem de venda no livro. ' + data.order.order_id))
//                         })
//                     },
//                     (data) => console.log('Erro ao inserir ordem de compra no livro. ' + data))
//             })
//         } else {
//             console.log('Ainda ta caro, vamos esperar')
//         }
//     }),
//     process.env.CRAWLER_INTERVAL
// )